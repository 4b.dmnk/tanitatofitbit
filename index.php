<?php
session_start();

$html_start = '<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Tanita to Fitbit</title>

    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>

  <body>

    <header>
      <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white">Your Data</h4>
              <p class="text-muted">Your data is just hold and transformed at this server till the import at fitbit is completed (or fails). We\'re never able to get your credentials or personal data, as all
			  processing happens through fitbit\'s official Web-API. We have no business connection to Tanita or Fitbit.</p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Contact</h4>
              <ul class="list-unstyled">
                <!--<li><a href="#" class="text-white">Follow on Twitter</a></li>
                <li><a href="#" class="text-white">Like on Facebook</a></li>
                <li><a href="#" class="text-white">Email me</a></li>-->
                <li><a href="https://community.fitbit.com/t5/notes/privatenotespage/tab/compose/note-to-user-id/12610781" target="_blank" class="text-white">Message me via Fitbit</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
          <a href="#" class="navbar-brand d-flex align-items-center">
            <!--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M448 64h-26c16.4 28.3 26 61 26 96 0 105.9-86.1 192-192 192S64 265.9 64 160c0-35 9.6-67.7 26-96H64C28.7 64 0 92.7 0 128v320c0 35.3 28.7 64 64 64h384c35.3 0 64-28.7 64-64V128c0-35.3-28.7-64-64-64zM256 320c88.4 0 160-71.6 160-160S344.4 0 256 0 96 71.6 96 160s71.6 160 160 160zm-.3-151.9l33.6-78.4c3.5-8.2 12.9-11.9 21-8.4s11.9 12.9 8.4 21L285 180.9c6.7 7.1 10.9 16.6 10.9 27.1 0 22.1-17.9 40-40 40s-40-17.9-40-40c.1-22 17.9-39.8 39.8-39.9z"/></svg>-->
            <strong>Tanita2Fitbit</strong>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>

    <main role="main">
    <section class="jumbotron ">
      <div class="container">';

  $html_end = '
  </div>
  </section>
  </main>

  <footer class="text-muted">
    <div class="container">
      <p class="float-right">
        <a href="#">Back to top</a>
      </p>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write(\'<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>\')</script>
  <script src="../../assets/js/vendor/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="../../assets/js/vendor/holder.min.js"></script>
</body>
</html>';

$html_0 = '<section class="jumbotron text-center">
  <div class="container">
    <h1 class="jumbotron-heading">Tanita BC-601 to Fitbit Import</h1>
    <p class="lead text-muted">Import your Tanita readings into Fitbits Weight and Bodyfat log.<br> Get a more complete view on your health and the effect of your training on your body data.</p>
    <p>
      <a href="?stage=1" class="btn btn-primary my-2">Start</a>
      <a href="https://www.fitbit.com/user/profile/apps" target="_new" class="btn btn-secondary my-2">Revoke access</a>
    </p>
  </div>
</section>

<section class="text-center">
  <h2>Your next steps:</h2>
</section>

<div class="album py-5 bg-light">
  <div class="container">

    <div class="row">
      <div class="col-md-3">
        <div class="card mb-3 box-shadow">
          <img class="card-img-top" src="./upload.png" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Step 1:<br> Upload the CSV from your SD-Card.</p>
            <div class="d-flex justify-content-between align-items-center">
             <!-- <div class="btn-group">
                <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
              </div>
              <small class="text-muted">9 mins</small>-->
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card mb-3 box-shadow">
          <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Step 2:<br> select which measures to provide to fitbit.</p>
            <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group">
                <!--<button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>-->
              </div>
              <small class="text-muted">not yet implemented</small>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card mb-3 box-shadow">
          <img class="card-img-top" src="./authorize.png" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Step 3:<br> Login & Allow this Service to put data into your weight and fat log. </p>
            <div class="d-flex justify-content-between align-items-center">

              <small class="text-muted">Skipped after the first time</small>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="card mb-3 box-shadow">
          <img class="card-img-top" src="./done.png" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Step 4:<br> Data is uploaded.</p>
            <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group">
              <!--  <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>-->
              </div>
              <small class="text-muted"></small>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>';

// TODO: depend on environment
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

require 'tanitaparser.php';

if (isset($_GET['stage'])) {
    $_SESSION['stage'] = $_GET['stage'];
} else {
    $_SESSION['stage'] = 0;
}

switch($_SESSION['stage']) {
case 3:
    showUploadResult();
    break;
case 2:
    echo $html_start;
    showUploadFilter();

    echo $html_end;
    break;
case 1:
    echo $html_start;
    showUpload();
    echo $html_end;
    break;
case 0:
default:
    echo $html_start;
    echo $html_0;
    echo $html_end;
    break;
}

function showUploadResult() {
  $_SESSION['tanitaarray'] = parseTanitaCSV($_FILES['tanitacsv']['tmp_name']);

  //echo(count($_FILES));
  //print_r($_FILES);
  //print_r($_SESSION['tanitaarray']);

  if(isset($_SESSION['tanitaarray']) && count($_SESSION['tanitaarray'] > 0)) {
    $redirUri = getenv('OAUTH_REDIRECT_URI');
    header("Location: $redirUri");
    die();
    include 'oauthPost.php';
  }
}


function showUpload() {
  ?>
  <form action="?stage=3" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="tanitacsv">Select your Tanita CSV from SD-Card:\TANITA\GRAPHV1\DATA\DATA{youruserID}.csv</label>
    <input type="file" class="form-control-file" name="tanitacsv" id="tanitacsv">

  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php
}

function showInfo() {

  echo "<h1>hi. here comes the info and doc page </h1>";
  echo "<a href='?stage=1'>continue</a>";
}
?>
