# Warning
This application was created in a hurry to scratch the itch of having a superior scale and no chance to import the measurements into fitbit.
Therefore there are some rough edges

# 12 Factor App
 - https://jolicode.com/blog/what-you-need-to-know-about-environment-variables-with-php


## Environment Variables

APP_URL = https://asdf.domain.tld/t2f/  

OAUTH_CLIENT_ID          => '123456',  
OAUTH_CLIENT_SECRET      => '123jk123jk123jlk',  
OAUTH_REDIRECT_URI       => 'https://asdf.domain.tld/t2f/oauthPost.php'  
OAUTH_REDIRECT_URI_INDEX       => 'https://asdf.domain.tld/t2f'  

## Getting the Environment Vars into PHP
Quite some research, but for the scenario NGINX : FastCGI (PHP-FPM)  
As nginx-directive for the vhost:
```
location @php { ##merge##
    fastcgi_param OAUTH_CLIENT_ID 'ASDF';
    fastcgi_param APP_ENV production; 
    fastcgi_param APP_URL 'https://t2f.yourDomain.net/';
    fastcgi_param OAUTH_CLIENT_SECRET '123jk123jk123jlk';
    fastcgi_param OAUTH_REDIRECT_URI 'https://tt2f.yourDomain.net/oauthPost.php';
    fastcgi_param OAUTH_REDIRECT_URI_INDEX 'https://t2f.yourDomain.net/';
}
```

Yep, there's still some redundancy that can & should be removed - i know ;)

 - https://www.howtoforge.com/community/threads/adding-environment-variables-to-a-php-fpm-pool.69719/

