<?php
//TODO: depend on environment
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

function parseTanitaCSV($filename, $debugPrint = false) {

  $retarr = array();
  $row = 1;
  if (($handle = fopen($filename, "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
          $num = count($data);
          $date = $data[13];
          $time = $data[15];
          $weightKG = $data[27];
          $bmi = $data[29];
          $fat = $data[31];

          $timestamp = DateTime::createFromFormat('d/m/Y H:i:s', "$date $time");
          $timestampStr = $timestamp->format('c');

          if($debugPrint) {
            echo "<p>D: $date T: $time TS: $timestampStr W: $weightKG BMI: $bmi</p>\n";
          }

          $retarr[] = array("timestamp" => $timestamp, "weight" => $weightKG, "bmi" => $bmi, "fat" => $fat);
          //echo "<p> $num Felder in Zeile $row: <br /></p>\n";

          $row++;
          //for ($c=0; $c < $num; $c++) {
          //    echo $data[$c] . "<br />\n";
          //}
      }
      fclose($handle);
  }

  if($debugPrint) {
    print_r($retarr);
  }

  return $retarr;

}

?>
