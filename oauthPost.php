<?php
/**
 * Uploads the three parameters.
 * 
 * @category   Fitness
 * @package    Tbd
 * @author     Dominik <4c.dmnk@gmail.com>
 * @license    GNU GPLv3
 * @link       https://gitlab.com/4b.dmnk/tanitatofitbit/
 * @phpversion 7.0+
 * 
 * @todo depend on app_environment if errors are reported or not
 */
//error_reporting(E_ALL);

//ini_set("display_errors", 1);

require __DIR__ . '/vendor/autoload.php';
//require 'tanitaparser.php';

use djchen\OAuth2\Client\Provider\Fitbit;

session_start();

$provider = new Fitbit(
    [
        'clientId'          => getenv('OAUTH_CLIENT_ID'),
        'clientSecret'      => getenv('OAUTH_CLIENT_SECRET'),
        'redirectUri'       => getenv('OAUTH_REDIRECT_URI')
    ]
);

// start the session
//session_start();

// If we don't have an authorization code then get one
if (!isset($_GET['code'])) {
    $options = [
    'scope' => ['weight']
    ];

    // Fetch the authorization URL from the provider; this returns the
    // urlAuthorize option and generates and applies any necessary parameters
    // (e.g. state).
    $authorizationUrl = $provider->getAuthorizationUrl($options);

    // Get the state generated for you and store it to the session.
    $_SESSION['oauth2state'] = $provider->getState();

    // Redirect the user to the authorization URL.
    header('Location: ' . $authorizationUrl);
    exit;

    // Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || array_key_exists('oauth2state', $_SESSION) && ($_GET['state'] !== $_SESSION['oauth2state'])) {
    unset($_SESSION['oauth2state']);
    exit('Invalid state');

} else {

    try {

        // Try to get an access token using the authorization code grant.
        $accessToken = $provider->getAccessToken(
            'authorization_code', 
            [
                'code' => $_GET['code']
            ]
        );

        // We have an access token, which we may use in authenticated
        // requests against the service provider's API.
        /*echo $accessToken->getToken() . "\n";
        echo $accessToken->getRefreshToken() . "\n";
        echo $accessToken->getExpires() . "\n";
        echo ($accessToken->hasExpired() ? 'expired' : 'not expired') . "\n";*/

        //without echos
        $accessToken->getToken();
        $accessToken->getRefreshToken();
        $accessToken->getExpires();

        // Using the access token, we may look up details about the
        // resource owner.
        /*  $resourceOwner = $provider->getResourceOwner($accessToken);

        var_export($resourceOwner->toArray());*/

        // The provider provides a way to get an authenticated API request for
        // the service, using the access token; it returns an object conforming
        // to Psr\Http\Message\RequestInterface.
        /*  $request = $provider->getAuthenticatedRequest(
            Fitbit::METHOD_GET,
            Fitbit::BASE_FITBIT_API_URL . '/1/user/-/profile.json',
            $accessToken,
            ['headers' => [Fitbit::HEADER_ACCEPT_LANG => 'en_US'], [Fitbit::HEADER_ACCEPT_LOCALE => 'en_US']]
            // Fitbit uses the Accept-Language for setting the unit system used
            // and setting Accept-Locale will return a translated response if available.
            // https://dev.fitbit.com/docs/basics/#localization
        );
        // Make the authenticated API request and get the parsed response.
        $response = $provider->getParsedResponse($request);*/

        doUpload($provider, $accessToken);
        showLogout();


        // If you would like to get the response headers in addition to the response body, use:
        //$response = $provider->getResponse($request);
        //$headers = $response->getHeaders();
        //$parsedResponse = $provider->parseResponse($response);

    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

        // Failed to get the access token or user details.
        exit($e->getMessage());

    }

}

function showLogout() 
{
    $_SESSION['stage'] = 0;
    $html_start = '<!doctype html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="../../../../favicon.ico">

      <title>Tanita to Fitbit</title>

      <!-- Bootstrap core CSS -->
      <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">

      <!-- Custom styles for this template -->
      <link href="album.css" rel="stylesheet">
    </head>

    <body>

      <header>
        <div class="collapse bg-dark" id="navbarHeader">
          <div class="container">
            <div class="row">
              <div class="col-sm-8 col-md-7 py-4">
                <h4 class="text-white">Your Data</h4>
                <p class="text-muted">Your data is just hold and transformed at this server till the import at fitbit is completed (or fails). We\'re never able to get your credentials or personal data, as all
  			  processing happens through fitbit\'s official Web-API. We have no business connection to Tanita or Fitbit.</p>
              </div>
              <div class="col-sm-4 offset-md-1 py-4">
                <h4 class="text-white">Contact</h4>
                <ul class="list-unstyled">
                  <!--<li><a href="#" class="text-white">Follow on Twitter</a></li>
                  <li><a href="#" class="text-white">Like on Facebook</a></li>
                  <li><a href="#" class="text-white">Email me</a></li>-->
                  <li><a href="https://community.fitbit.com/t5/notes/privatenotespage/tab/compose/note-to-user-id/12610781" target="_blank" class="text-white">Message me via Fitbit</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="navbar navbar-dark bg-dark box-shadow">
          <div class="container d-flex justify-content-between">
            <a href="#" class="navbar-brand d-flex align-items-center">
              <!--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M448 64h-26c16.4 28.3 26 61 26 96 0 105.9-86.1 192-192 192S64 265.9 64 160c0-35 9.6-67.7 26-96H64C28.7 64 0 92.7 0 128v320c0 35.3 28.7 64 64 64h384c35.3 0 64-28.7 64-64V128c0-35.3-28.7-64-64-64zM256 320c88.4 0 160-71.6 160-160S344.4 0 256 0 96 71.6 96 160s71.6 160 160 160zm-.3-151.9l33.6-78.4c3.5-8.2 12.9-11.9 21-8.4s11.9 12.9 8.4 21L285 180.9c6.7 7.1 10.9 16.6 10.9 27.1 0 22.1-17.9 40-40 40s-40-17.9-40-40c.1-22 17.9-39.8 39.8-39.9z"/></svg>-->
              <strong>Tanita2Fitbit</strong>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
        </div>
      </header>

      <main role="main">
      <section class="jumbotron ">
        <div class="container">';

    $html_end = '
    </div>
    </section>
    </main>

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write(\'<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>\')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
  </body>
  </html>';
  
    echo $html_start;
    echo "<h2>Upload successful<h2>";
    echo '<a href="./?stage=0" class="btn btn-primary my-2">Back to Start</a>';
    echo $html_end;
}

function doUpload($provider, $accessToken) 
{

    foreach ($_SESSION['tanitaarray'] as $entry => $measureArray) {
        postWeight($provider, $accessToken, $measureArray["timestamp"], $measureArray["weight"]);
        postFat($provider, $accessToken, $measureArray["timestamp"], $measureArray["fat"]);
    }
}


function postFat($provider, $accessToken, $timestamp, $fat) 
{

    $parameters = "fat=$fat&date=". date_format($timestamp, "Y-m-d") . "&time=". date_format($timestamp, "H:m:s")."&source=tanita";

    $fatRequest = $provider->getAuthenticatedRequest(
        Fitbit::METHOD_POST,
        Fitbit::BASE_FITBIT_API_URL . '/1/user/-/body/log/fat.json' . "&" . $parameters,
        $accessToken,
        ['headers' => [Fitbit::HEADER_ACCEPT_LANG => 'de_DE'],
        [Fitbit::HEADER_ACCEPT_LOCALE => 'de_DE']
        ]
    );

    $fatResponse = $provider->getParsedResponse($fatRequest);
    return $fatResponse;
}

function postWeight($provider, $accessToken, $timestamp, $weight) 
{
    $parameters = "weight=$weight&date=". date_format($timestamp, "Y-m-d") . "&time=". date_format($timestamp, "H:m:s")."&source=tanita";
    $weightRequest = $provider->getAuthenticatedRequest(
        Fitbit::METHOD_POST,
        Fitbit::BASE_FITBIT_API_URL . '/1/user/-/body/log/weight.json' . "&" . $parameters,
        $accessToken,
        ['headers' => [Fitbit::HEADER_ACCEPT_LANG => 'de_DE'],
          [Fitbit::HEADER_ACCEPT_LOCALE => 'de_DE'] /*,
          ['body' => "weight=$weight&date=". date_format($timestamp, "Y-m-d") . "&time=". date_format($timestamp, "HH:mm:ss")
          ]*/
        ]
    );

    $weightResponse = $provider->getParsedResponse($weightRequest);
    return ($weightResponse);
}